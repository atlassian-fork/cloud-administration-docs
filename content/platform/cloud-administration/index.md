---
title: Index page for Cloud administration API Documentation
platform: platform
product: cloud-administration
category: customerguide
subcategory: index
date: "2018-11-7"
---
# Index page for Cloud administration API Documentation

Congratulations on creating your documentation set for Cloud administration API!

This is the index page for your documentation. In order to continue with your documentation
journey you should continue to read the developer.atlassian.com Writing Toolkit; specifically
[Organizing your docs](https://developer.atlassian.com/platform/writing-toolkit/organizing-your-docs/).

In order to learn how to make a great index page you can use the
[Index page template](/platform/cloud-administration/templates/index-page-template/).

## Example screenshot

![screenshot example](/platform/cloud-administration/images/placeholder.png)
