---
title: "Concept page template"
platform: platform
product: cloud-administration
category: devguide
subcategory: templates
date: "2018-11-7"
---

[This template is sourced from the Writing Toolkit.](https://developer.atlassian.com/platform/writing-toolkit/concept-page/)

Instructions for using this template:

 - Remove the HTML comments.
 - Remove optional sections that you are not using.
 - Ensure you update the YAML metadata (above).
 - Update headings, page title, etc.

# Concept page title

*Start with a brief introduction.*

## Second heading

The body includes lots of context and best practices.

Be sure to define product-specific terms in as human a language as possible.

Include visuals to illustrate complex concepts.

*Optional. Concept pages are great for videos and short animations when appropriate.*