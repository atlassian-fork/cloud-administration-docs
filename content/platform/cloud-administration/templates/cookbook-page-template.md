---
title: "Cookbook title"
platform: platform
product: cloud-administration
category: devguide
subcategory: templates
date: "2018-11-7"
---

[This template is sourced from the Writing Toolkit.](https://developer.atlassian.com/platform/writing-toolkit/cookbook-page/)

Instructions for using this template:

- Remove the HTML comments.
- Remove optional sections that you are not using.
- Ensure you update the YAML metadata (above).
- Update headings, page title, etc.

# Cookbook title

*Start with a brief introduction to the cookbook.*

## Add a recipe title

Use imperative verbs in recipe headings (e.g., use 'Create an app', not 'Creating an app').

Description:

- Describe the problem/scenario as well as how you're solving it.
- Limit the description to one or two paragraphs.
- Specify language-specific solutions if you cannot use a common language.

Code:

- Only include one code sample.
- Whenever possible, use a common language or indicate if the solution is language-specific.

If you have more to write or more code samples, then you should create a tutorial instead.

A copy/pasteable recipe template appears below.

## Add a recipe title

Replace this description.

``` javascript
<add a code sample>
```